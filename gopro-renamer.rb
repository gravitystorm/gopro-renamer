#!/usr/bin/ruby -w
# frozen_string_literal: true

# A small utility for renaming gopro video files in order to keep my sanity
# GoPro uses a strange naming convention by default, which destroys any kind
# of file sorting. This is frustrating.

# 2020-01-15-something-0014-b.mp4/thm/lrv

require 'exiftool'
require 'optparse'
require 'ostruct'

options = OpenStruct.new

optparse = OptionParser.new do |opts|
  opts.banner = 'Usage: gopro-renamer.rb directory'

  opts.on('-lLABEL', '--label=LABEL', 'Label these videos') do |l|
    options.label = l
  end

  opts.on('--really', 'Really do the renaming') do |r|
    options.really = r
  end

  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts
    exit
  end
end

optparse.parse!

if ARGV.length != 1
  optparse.abort('Error: directory required')
  exit(-1)
end

directory = ARGV[0]

files = Dir.entries(directory)

targets = []

files.each do |filename|
  # Skip files that are not in the correct naming convention, i.e.
  # GH000000.THM GH000000.MP4 GL000000.LRV
  case filename
  when /^GH[0-9]{6}.MP4$/
    targets.push filename
  when /^GH[0-9]{6}.THM$/
    # silently ignore
  when /^GL[0-9]{6}.LRV$/
    # silently ignore
  else
    puts "skipping filename #{filename}"
  end
end

puts "#{targets.length} files to process"

targets.sort.each do |target|
  # create the destination name for the renaming
  e = Exiftool.new(File.join(directory, target))
  m = /^GH(?<chapter>[0-9]{2})(?<file_num>[0-9]{4}).MP4/.match(target)
  dest_filename = e[:create_date_civil].to_s
  dest_filename += "-#{options.label}" if options.label
  dest_filename += "-#{m[:file_num]}"
  dest_filename += "-#{m[:chapter]}"
  basename = File.basename(target, '.MP4')
  puts "rename #{basename}.MP4 -> #{dest_filename}.mp4"
  puts "rename #{basename}.THM -> #{dest_filename}.thm"
  # LRVs have a different basename
  lrv_basename = basename.dup
  lrv_basename[1] = 'L'
  puts "rename #{lrv_basename}.LRV -> #{dest_filename}.lrv"

  next unless options.really

  source = File.join(directory, "#{basename}.MP4")
  dest = File.join(directory, "#{dest_filename}.mp4")
  File.rename(source, dest)

  source = File.join(directory, "#{basename}.THM")
  dest = File.join(directory, "#{dest_filename}.thm")
  File.rename(source, dest)

  source = File.join(directory, "#{lrv_basename}.LRV")
  dest = File.join(directory, "#{dest_filename}.lrv")
  File.rename(source, dest)
end
