# gopro-renamer

This is a small utility to rename video files from gopros into a more helpful format.

It takes files with names like:

```
GH000000.THM GH000000.MP4 GL000000.LRV
```

and uses the video EXIF data to rename them to files like:

```
2021-04-17-0023-01.mp4
2021-04-17-0023-01.thm
2021-04-17-0023-01.lrv
```

The advantages are:

* The files can be easily sorted, since the LRV files are now beside the THM and MP4 files
* The filenames contain the date, so everything is automatically sorted in chronolical order
* The filename contains the chapter last, so that file `0023 chapter 01` comes first, then `0023 chapter 02` comes next.

## Labels

You can specify a label for a batch of videos. So if you have a bunch of videos
from a day of mountain biking, run with `--label=mtb` and the label will appear after the data e.g.

```
2021-04-17-mtb-0023-01.mp4
```

This helps if you have a lot of files and can't remember what you were doing each day - particularly useful if the thumbnail images aren't loading.

## Really

You need to run the `--really` flag to really rename the files. This is because it's not exactly the most robust utility (no tests yet!) so I'm being a bit cautious for now.
